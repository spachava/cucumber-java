package org.com.cucumber;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by eki1h3v on 11/20/2014.
 */
public class BasePage {

    private WebDriver driver = new FirefoxDriver();

    @Before
    public void setUp() {
        driver.get("http://seleniumhq.org");
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
