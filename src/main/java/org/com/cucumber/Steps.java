package org.com.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

/**
 * Created by Srinivas on 10/25/2014.
 */
public class Steps {

    @Given("^I am on home page$")
    public void start() throws Throwable {
        System.out.println("Start");
    }

    @When("^I click on \"(.*?)\"$")
    public void clickBy(String arg1) throws Throwable {
        System.out.println("Click");
    }

    @Then("^I should validate \"(.*?)\"$")
    public void validate(String arg1) throws Throwable {
        System.out.println("Finish");
    }
}
