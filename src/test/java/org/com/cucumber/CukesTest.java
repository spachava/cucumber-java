package org.com.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Srinivas on 10/11/2014.
 */

@RunWith(Cucumber.class)
@CucumberOptions(format = {"html:target","pretty","json:target/cucumber.json"},
        features = {"src/test/resources/features"},
        tags = {"@sanity"})
public class CukesTest {
}
