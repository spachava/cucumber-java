@sanity
@smoke
@system
@regression

Feature: SeleniumHQ Site

Scenario Outline: Open SeleniumHQ Site
  Given I am on home page
  When I click on "<Link>"
  Then I should validate "<Header>"

  Examples:
    | Link       | Header           |
    | dummy      | dummy.header     |