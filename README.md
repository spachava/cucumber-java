# README #

This is Base Framework to start implementing Automation Scenarios with Cucumber BDD and Java.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Basic Installations required to start:

 * JDK
 * Maven

How to execute this framework?

Maven command to run tests

 * mvn clean install

How to see reports?

Test results are stored in target folder "cucumber-html-reports"

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact